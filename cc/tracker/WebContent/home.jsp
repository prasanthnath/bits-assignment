<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="tracker.model.Todo" %>
<%@ page import="tracker.dao.Dao" %>

<!DOCTYPE html>


<%@page import="java.util.ArrayList"%>

<html>

<head>
	<title>Task Tracker</title>
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<meta charset="utf-8">
</head>

<body>
	<%
	List<Todo> todos = Dao.listTodos();
	%>
	
	<div class="header">
		<div style="float: left;"><img src="images/todo.png" /></div>
		<div style="float: left;" class="headline">Task Tracker</div>
	</div>
	<div class="new-task">
		<div class="head">New Task</div>
		<form action="/new" method="post" accept-charset="utf-8">
			<div class="body">
				<div>
					<label for="summary">Summary</label>
					<input type="text" name="summary" id="summary" />
				</div>
				<div>
					<label for="description">Description</label>
					<textarea rows="4" name="description" id="description"></textarea>
				</div>
				<div>
					<label for="user">User</label>
					<input type="text" name="user" id="user" />
				</div>
			</div>
			<div class="footer head">
				<input type="submit" value="Create" />
			</div>
		</form>
	</div>
	<div class="total-tasks">
		You have a total number of <%= todos.size() %> tasks.
	</div>

	<div class="task-list">
		<table>
			<tr>
				<th>Summary </th>
				<th>Description</th>
				<th>User</th>
				<th>Done</th>
				<th>Delete</th>
			</tr>
			<% for (Todo todo : todos) {%>
			<tr>
				<td>
					<%=todo.getShortDescription()%>
				</td>
				<td>
					<%=todo.getLongDescription()%>
				</td>
				<td>
					<%=todo.getUser()%>
				</td>
				<td>
					<input type="checkbox" <%if(todo.isFinished()){%>checked<%}%>>
				</td>
				<td>
					<a class="done" href="/done?id=<%=todo.getId()%>&action=delete">Delete</a>
				</td>
			</tr>
			<%}%> </table> </div> </body> </html>
