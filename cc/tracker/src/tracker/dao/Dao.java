package tracker.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import tracker.model.Todo;


public class Dao {
	private static AtomicInteger counter = new AtomicInteger();
	private static List<Todo> todos = new ArrayList<Todo>();
	
	public static List<Todo> listTodos() {
		return todos;
	}

	public static void add(String summary, String description, String user) {
		Todo todo = new Todo(counter.incrementAndGet(), summary, description, user);
		todos.add(todo);
	}
	
	public static void complete(Integer id) {
		for(Iterator<Todo> iter = todos.iterator(); iter.hasNext(); ) {
			Todo next = iter.next();
			if(next.getId().equals(id)) {
				next.setFinished(true);
			}
		}
	}

	public static void remove(Integer id) {
		for(Iterator<Todo> iter = todos.iterator(); iter.hasNext(); ) {
			Todo next = iter.next();
			if(next.getId().equals(id)) {
				iter.remove();
			}
		}
	}
}
