				
package tracker;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tracker.dao.Dao;


public class ServletRemoveTodo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String id = req.getParameter("id");
		String action = req.getParameter("action");
		
		if("delete".equals(action)) {
			Dao.remove(Integer.parseInt(id));
		} else {
			Dao.complete(Integer.parseInt(id));
		}
		resp.sendRedirect("/home.jsp");
	}
}

