package tracker.model;

/**
 * Model class which will store the Todo Items
 */
public class Todo {
	private Integer id;

	private String summary;
	private String description;
	private String user;
	boolean finished;

	public Todo(Integer id, String summary, String description, String url) {
		this.id = id;
		this.summary = summary;
		this.description = description;
		this.user = url;
		finished = false;
	}

	public Integer getId() {
		return id;
	}

	public String getShortDescription() {
		return summary;
	}

	public void setShortDescription(String shortDescription) {
		this.summary = shortDescription;
	}

	public String getLongDescription() {
		return description;
	}

	public void setLongDescription(String longDescription) {
		this.description = longDescription;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

}
