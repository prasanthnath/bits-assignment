package tracker.model;

public class MyUser {
	private String id;
	private String userId;
	private String email;

	public MyUser(String userId, String email) {
		this.id = userId;
		this.userId = userId;
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public String getUserId() {
		return userId;
	}

	public String getEmail() {
		return email;
	}

}
