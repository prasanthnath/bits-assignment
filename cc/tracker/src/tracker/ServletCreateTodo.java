package tracker;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tracker.dao.Dao;


@SuppressWarnings("serial")
public class ServletCreateTodo extends HttpServlet {
	private static final Logger log = Logger.getLogger(ServletCreateTodo.class.getName());

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		log.severe("Creating new task ");
		String summary = checkNull(req.getParameter("summary"));
		String description = checkNull(req.getParameter("description"));
		String user = checkNull(req.getParameter("user"));
		Dao.add(summary, description, user);
		resp.sendRedirect("/home.jsp");
	}

	private String checkNull(String s) {
		return s == null ? "" : s;
	}
}
