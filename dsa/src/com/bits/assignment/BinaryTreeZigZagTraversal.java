package com.bits.assignment;

import com.bits.assignment.model.BinaryTreeNode;
import com.bits.assignment.model.BinaryTreePrinter;

import java.util.Stack;

/**
 * Question : Zig Zag Tree Traversal: Give an algorithm to traverse a binary tree in Zig Zag order. For example,
 *            the output for the tree below should be: 1 3 2 4 5 6 7.
 */
public class BinaryTreeZigZagTraversal {
    public static <T> void traverseZigZag(BinaryTreeNode<T> node) {
        // the flag that makes it zig zag. we keep flipping this after each iteration
        boolean leftToRight = true;
        // this is the stack from which we traverse
        Stack<BinaryTreeNode<T>> current = new Stack<>();
        // this is the stack that we prepare for the next row. When it is fully prepared it becomes the current row.
        // which means, when we identify all the elements in the next row then we can make the next row as current
        // ans start traversing that
        Stack<BinaryTreeNode<T>> next = new Stack<>();
        current.push(node);

        while (!current.isEmpty()) {
            BinaryTreeNode<T> latestNode = current.pop();
            System.out.print(latestNode.getData() + " ");
            if (leftToRight) {
                // left to right case
                // first left node then right node
                if (latestNode.getLeft() != null)
                    next.push(latestNode.getLeft());
                if (latestNode.getRight() != null)
                    next.push(latestNode.getRight());
            } else {
                // right to left case
                // first right node then left node
                if (latestNode.getRight() != null)
                    next.push(latestNode.getRight());
                if (latestNode.getLeft() != null)
                    next.push(latestNode.getLeft());
            }
            // we are done with the current row, lets see what have we collected for the next iteration
            if (current.isEmpty()) {
                // next one is again going to be opposite of this one
                leftToRight = !leftToRight;
                current = next;
                next = new Stack<>();
            }
        }
    }

    public static void main(String[] args) {
        BinaryTreeNode<String> tree = createBinaryTree();
        BinaryTreePrinter.printNode(tree);
        traverseZigZag(tree);
    }

    private static BinaryTreeNode<String> createBinaryTree() {
        BinaryTreeNode<String> root = new BinaryTreeNode("1");
        BinaryTreeNode<String> twoNode = root.insert("2");
        BinaryTreeNode<String> threeNode = root.insert("3");
        twoNode.insert("4");
        twoNode.insert("5");
        threeNode.insert("6");
        threeNode.insert("7");
        return root;
    }
}
