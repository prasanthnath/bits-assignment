package com.bits.assignment.model;

public class BSTNode<T extends Comparable> extends BinaryTreeNode<T> {
    public BSTNode(T content) {
        super(content);
    }

    public BSTNode(T content, BinaryTreeNode<T> left, BinaryTreeNode<T> right) {
        super(content, left, right);
    }

    @Override
    public BSTNode getLeft() {
        return (BSTNode) super.getLeft();
    }

    @Override
    public BSTNode getRight() {
        return (BSTNode) super.getRight();
    }

    public BinaryTreeNode insert(T s) {
        if (data.compareTo(s) < 0) {
            // if bigger then go right
            if (right == null) {
                setRight(new BSTNode(s));
                return right;
            } else
                return right.insert(s);
        } else {
            // if smaller then go left
            if (left == null) {
                setLeft(new BSTNode(s));
                return left;
            } else
                return left.insert(s);
        }
    }
}
