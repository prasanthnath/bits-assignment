package com.bits.assignment.model;

public class BinaryTreeNode<T> {  //standard Binary Tree node
    protected T data;
    protected BinaryTreeNode left;
    protected BinaryTreeNode right;

    public BinaryTreeNode(T content) {
        this(content, null, null);
    }

    BinaryTreeNode(T content, BinaryTreeNode<T> left, BinaryTreeNode<T> right) {
        this.left = left;
        this.right = right;
        data = content;
    }

    public BinaryTreeNode getLeft() {
        return left;
    }

    public void setLeft(BinaryTreeNode left) {
        this.left = left;
    }

    public BinaryTreeNode getRight() {
        return right;
    }

    public void setRight(BinaryTreeNode right) {
        this.right = right;
    }

    public T getData() {
        return data;
    }

    /* below is standard Binary Search tree insert code, creates the tree */

    //public BinaryTreeNode insertBST(BinaryTreeNode<T> parent, T s) {
        /*if (parent == null) {
            return new BinaryTreeNode(s);
        } else {
            if (s.compareTo(parent.data.toString()) < 0)
                parent.left = insertBST(parent.left, s);
            else
                parent.right = insertBST(parent.right, s);
            return parent;
        }*/
    //}

    public BinaryTreeNode insert(T s) {
        if (this.left == null) {
            this.left = new BinaryTreeNode(s);
            return this.left;
        } else if (this.right == null) {
            this.right = new BinaryTreeNode(s);
            return this.right;
        }
        return null;
    }
}