package com.bits.assignment;

import com.bits.assignment.model.BinaryTreePrinter;
import com.bits.assignment.model.BinaryTreeNode;

/**
 * Question : Give an algorithm for converting a tree to its mirror. Mirror of a tree is another tree with left
 *            and right children of all non-leaf nodes interchanged.
 */
public class BinaryTreeMirrorImage {

    public static <T> void convertToMirrorImage(BinaryTreeNode<T> binaryTreeNode) {
        // in a mirror image left becomes right and right becomes left. so we recursively
        // make the right child as the left child and vice versa in a depth first manner
        if (binaryTreeNode != null) {
            // as it is depth first we keep going down the tree first
            convertToMirrorImage(binaryTreeNode.getLeft());
            convertToMirrorImage(binaryTreeNode.getRight());
            // once the left and right sub-trees are taken care of we just swich the left anf right children of the
            // current node.
            BinaryTreeNode<T> tempNode = binaryTreeNode.getLeft();
            binaryTreeNode.setLeft(binaryTreeNode.getRight());
            binaryTreeNode.setRight(tempNode);
        }
    }

    public static void main(String[] args) {
        BinaryTreeNode<String> tree = createBinaryTree();
        BinaryTreePrinter.printNode(tree);
        convertToMirrorImage(tree);
        BinaryTreePrinter.printNode(tree);
    }

    private static BinaryTreeNode<String> createBinaryTree() {
        BinaryTreeNode<String> root = new BinaryTreeNode("1");
        BinaryTreeNode<String> twoNode = root.insert("2");
        BinaryTreeNode<String> threeNode = root.insert("3");
        twoNode.insert("4");
        BinaryTreeNode<String> fiveNode = twoNode.insert("5");
        fiveNode.insert("7");
        fiveNode.insert("8");
        threeNode.insert("9");

        return root;
    }
}
