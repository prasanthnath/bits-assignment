package com.bits.assignment;

import com.bits.assignment.model.BSTNode;
import com.bits.assignment.model.BinaryTreePrinter;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 */
public class BSTComparator {

    public static <T extends Comparable> boolean compareBSTs(BSTNode<T> root1, BSTNode<T> root2) {
        // in order traversal of BST will give the values in sorted order
        // here we collect the sorted values in queues
        // time complexity of this step is O(logn) and space complexity O(n)
        Queue<T> sortedValues1 = new LinkedList<>();
        Queue<T> sortedValues2 = new LinkedList<>();
        inorderTraversal(root1, sortedValues1);
        inorderTraversal(root2, sortedValues2);
        // now compare the sorted values, if the sizes do not match then they are not same anyway
        if (sortedValues1.size() != sortedValues2.size())
            return false;
        // else poll one from each queue and compare. Time complexity of this step is O(n) and space complexity
        // is O(1)
        while (!sortedValues1.isEmpty()) {
            if (!sortedValues1.poll().equals(sortedValues2.poll()))
                return false;
        }
        return true;
    }

    private static <T extends Comparable> void inorderTraversal(BSTNode<T> node, Queue<T> sortedData) {
        if (node == null)
            return;
        inorderTraversal(node.getLeft(), sortedData);
        sortedData.offer(node.getData());
        inorderTraversal(node.getRight(), sortedData);
    }

    public static void main(String[] args) {
        BSTNode<Integer> root1 = new BSTNode<>(10);
        root1.insert(5);
        root1.insert(20);
        root1.insert(15);
        root1.insert(30);
        BinaryTreePrinter.printNode(root1);
        BSTNode<Integer> root2 = new BSTNode<>(10);
        root2.insert(20);
        root2.insert(15);
        root2.insert(30);
        root2.insert(5);
        BinaryTreePrinter.printNode(root2);
        BSTNode<Integer> root3 = new BSTNode<>(20);
        root3.insert(30);
        root3.insert(5);
        root3.insert(10);
        root3.insert(15);
        BinaryTreePrinter.printNode(root3);
        System.out.println("root1 == root2 ? " + compareBSTs(root1, root2));
        System.out.println("root2 == root3 ? " + compareBSTs(root2, root3));
        System.out.println("root3 == root1 ? " + compareBSTs(root3, root1));
        BSTNode<Integer> root4 = new BSTNode<>(10);
        root4.insert(5);
        root4.insert(20);
        root4.insert(15);
        root4.insert(30);
        BinaryTreePrinter.printNode(root4);
        BSTNode<Integer> root5 = new BSTNode<>(10);
        root5.insert(5);
        root5.insert(30);
        root5.insert(20);
        root5.insert(5);
        BinaryTreePrinter.printNode(root5);
        System.out.println("root4 == root5 ? " + compareBSTs(root4, root5));
    }
}
