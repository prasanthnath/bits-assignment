package com.bits.assignment;

import java.util.Arrays;

public class PlatformCounter {

    // Returns minimum number of platforms required
    private static int countPlatform(int arr[], int dep[], int n) {
        // Sort arrival and departure arrays
        Arrays.sort(arr);
        Arrays.sort(dep);

        // platNeeded indicates number of platforms
        // needed at a time
        int platNeeded = 1, result = 1;
        int i = 1, j = 0;

        while (i < n && j < n) {
            // If next event in sorted order is arrival,
            // increment count of platforms needed
            if (arr[i] <= dep[j]) {
                platNeeded++;
                i++;
                // Update result if needed
                if (platNeeded > result)
                    result = platNeeded;
            }
            else {
                // Else decrement count of platforms needed
                platNeeded--;
                j++;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int arr[] = {900, 915, 1030, 1045};
        int dep[] = {930, 1300, 1100, 1145};
        int n = arr.length;
        System.out.println("Minimum number of platforms required = " + countPlatform(arr, dep, n));
    }
} 
