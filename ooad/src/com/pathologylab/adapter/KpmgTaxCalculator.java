package com.pathologylab.adapter;

import com.pathologylab.model.ItemDetail;

public class KpmgTaxCalculator{

    public double calculateTax(ItemDetail itemDetail) {
        double percentage;
        switch (itemDetail.getItemCode()){
            case "HSN01":
                percentage = 6;
                break;
            case "HSN02":
                percentage = 3.5;
                break;
            default:
                percentage = 4;
                break;
        }
        return itemDetail.getQuantity() * percentage * itemDetail.getAmountPerUnit() / 100;
    }
}
