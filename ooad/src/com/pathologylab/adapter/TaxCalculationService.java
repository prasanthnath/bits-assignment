package com.pathologylab.adapter;

import com.pathologylab.model.PathologyTest;

public interface TaxCalculationService {

    double calculateTax(PathologyTest test);

}
