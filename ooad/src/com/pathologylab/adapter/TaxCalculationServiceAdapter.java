package com.pathologylab.adapter;

import com.pathologylab.model.HealthProfile;
import com.pathologylab.model.ItemDetail;
import com.pathologylab.model.PathologyTest;

public class TaxCalculationServiceAdapter implements TaxCalculationService{

    private KpmgTaxCalculator kpmgTaxCalculator;

    public TaxCalculationServiceAdapter(KpmgTaxCalculator kpmgTaxCalculator){
        this.kpmgTaxCalculator = kpmgTaxCalculator;
    }

    @Override
    public double calculateTax(PathologyTest test) {
        double taxedAmt = 0;
        if(test instanceof HealthProfile){
            HealthProfile healthProfile = (HealthProfile)test;
            for (PathologyTest testComponent : healthProfile.getTestList()) {
                ItemDetail itemDetail = getItemDetail(testComponent);
                taxedAmt = taxedAmt + kpmgTaxCalculator.calculateTax(itemDetail);
            }
        } else {
            ItemDetail itemDetail = getItemDetail(test);
            taxedAmt = kpmgTaxCalculator.calculateTax(itemDetail);
        }
        return taxedAmt;
    }

    private ItemDetail getItemDetail(PathologyTest test) {
        ItemDetail itemDetail = new ItemDetail();
        itemDetail.setItemCode(test.getHsnCode());
        itemDetail.setItemName(test.getName());
        itemDetail.setItemDescription(test.getDescription());
        itemDetail.setQuantity(1);
        itemDetail.setAmountPerUnit(test.getPayableAmount());
        return itemDetail;
    }
}
