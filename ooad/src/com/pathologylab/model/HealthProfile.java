package com.pathologylab.model;

import java.util.ArrayList;
import java.util.List;

public class HealthProfile implements PathologyTest {
    private String profileName;
    private String profileDescription;
    private List<PathologyTest> testList = new ArrayList<>();

    @Override
    public String getName() {
        return profileName;
    }

    public void setName(String name) {
        this.profileName = name;
    }

    @Override
    public String getDescription() {
        return profileDescription;
    }

    public void setDescription(String profileDescription) {
        this.profileDescription = profileDescription;
    }

    @Override
    public double getPayableAmount() {
        System.out.println("Calculating payable amount for "+profileName);
        double payableAmt = 0;
        for (PathologyTest test : testList) {
            System.out.println(" -Component test: "+test.getName()+", Payable amount: "+test.getPayableAmount());
            payableAmt = payableAmt + test.getPayableAmount();
        }
        return payableAmt;
    }

    @Override
    public String getHsnCode() {
        throw new UnsupportedOperationException("Method can't be called on Composite element");
    }

    public List<PathologyTest> getTestList() {
        return testList;
    }

    public void addTest(PathologyTest pathologyTest){
        testList.add(pathologyTest);
    }
}
