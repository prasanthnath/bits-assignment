package com.pathologylab.model;

public class BloodTest implements PathologyTest {
    private String testCode;
    private String testName;
    private String testDescription;
    private double payableAmt;
    private String hsnCode;

    @Override
    public String getName() {
        return testName;
    }

    public void setName(String testName) {
        this.testName = testName;
    }

    public String getCode() {
        return testCode;
    }

    public void setCode(String testCode) {
        this.testCode = testCode;
    }

    @Override
    public String getDescription() {
        return testDescription;
    }

    public void setDescription(String testDescription) {
        this.testDescription = testDescription;
    }

    @Override
    public double getPayableAmount() {
        return payableAmt;
    }

    public void setPayableAmount(double payableAmt) {
        this.payableAmt = payableAmt;
    }

    @Override
    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }
}
