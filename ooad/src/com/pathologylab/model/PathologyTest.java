package com.pathologylab.model;

public interface PathologyTest {
    String getName();
    String getDescription();
    double getPayableAmount();
    String getHsnCode();
}
