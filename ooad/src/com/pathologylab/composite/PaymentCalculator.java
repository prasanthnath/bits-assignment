package com.pathologylab.composite;

import com.pathologylab.model.BloodTest;
import com.pathologylab.model.HealthProfile;
import com.pathologylab.model.PathologyTest;

public class PaymentCalculator {

    public double calcPayableAmt(PathologyTest pathologyTest){
        // Can check if the system is offering is any discount and subtract it from
        // the payable amount
        return pathologyTest.getPayableAmount();
    }

    public static void main(String[] args) {
        BloodTest bloodTest1 = new BloodTest();
        bloodTest1.setName("Haemoglobin Count Test");
        bloodTest1.setDescription("Cholesterol level test");
        bloodTest1.setCode("BT01");
        bloodTest1.setHsnCode("HSN01");
        bloodTest1.setPayableAmount(400);

        BloodTest bloodTest2 = new BloodTest();
        bloodTest2.setName("Iron deficiency test");
        bloodTest2.setDescription("Iron deficiency test");
        bloodTest2.setCode("BT02");
        bloodTest2.setHsnCode("HSN02");
        bloodTest2.setPayableAmount(500);

        HealthProfile healthProfile = new HealthProfile();
        healthProfile.setName("Arogyam1 Profile");
        healthProfile.setDescription("Basic Tests");
        healthProfile.addTest(bloodTest1);
        healthProfile.addTest(bloodTest2);

        PaymentCalculator paymentCalculator = new PaymentCalculator();
        System.out.println("---- Individual Tests cost------------");
        System.out.println("Haemoglobin Count Test cost: "+paymentCalculator.calcPayableAmt(bloodTest1));
        System.out.println("Iron deficiency test cost: "+paymentCalculator.calcPayableAmt(bloodTest2));
        System.out.println("---- Health Profiles cost------------");
        System.out.println("Arogyam1 Profile cost: "+paymentCalculator.calcPayableAmt(healthProfile));
    }
}
